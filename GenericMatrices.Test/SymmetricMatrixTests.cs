﻿using NUnit.Framework;
using GenericMatrices.EventArguments;
using System;

namespace GenericMatrices.Test
{
    public class SymmetricMatrixTests
    {
        private int[][] initialDataIntIncorrectRows;

        private int[][] initialDataIntIncorrectColumns;

        private int[][] intialDataInt1;

        [SetUp]
        public void Setup()
        {
            this.intialDataInt1 = new int[][]
            {
            new int[] { 1, 2, 3, 4, 5 },
               new int[] { 3, 4, 5, 6 },
                  new int[] { 5, 6, 7 },
                     new int[] { 7, 8 },
                        new int[] { 9 }
            };

            this.initialDataIntIncorrectRows = new int[][]
            {
            new int[] { 1, 2, 3, 4, 5 },
               new int[] { 3, 4, 5, 6 },
                     new int[] { 7, 8 },
                        new int[] { 9 }
            };

            this.initialDataIntIncorrectColumns = new int[][]
            {
            new int[] { 1, 2, 3, 4, 5 },
                  new int[] { 4, 5, 6 },
                  new int[] { 5, 6, 7 },
                     new int[] { 7, 8 },
                        new int[] { 9 }
            };
        }

        [Test]
        public void ConstructorTest()
        {
            var symmetricMatrix = new SymmetricMatrix<int>(intialDataInt1);

            Assert.AreEqual(symmetricMatrix.SizeOfSide, intialDataInt1.Length);
            Assert.AreEqual(symmetricMatrix[1, 2], symmetricMatrix[2, 1]);
        }

        [Test]
        public void ConstructorTestsThrowArgumentExceptionIncorrectRows()
        {
            Assert.Throws<ArgumentException>(() => new SquareMatrix<int>(this.initialDataIntIncorrectRows));
        }

        [Test]
        public void ConstructorTestsThrowArgumentExceptionIncorrectColumns()
        {
            Assert.Throws<ArgumentException>(() => new SquareMatrix<int>(this.initialDataIntIncorrectRows));
        }
    }
}