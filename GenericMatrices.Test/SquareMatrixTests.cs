using NUnit.Framework;
using GenericMatrices.EventArguments;
using System;

namespace GenericMatrices.Test
{
    public class SquareMatrixTests
    {
        private int[][] initialDataIntIncorrectRows;

        private int[][] initialDataIntIncorrectColumns;

        private int[][] intialDataInt1;

        [SetUp]
        public void Setup()
        {
            this.intialDataInt1 = new int[][]
            {
            new int[] { 1, 2, 3, 4, 5 },
            new int[] { 2, 3, 4, 5, 6 },
            new int[] { 3, 4, 5, 6, 7 },
            new int[] { 4, 5, 6, 7, 8 },
            new int[] { 5, 6, 7, 8, 9 }
            };

            this.initialDataIntIncorrectRows = new int[][]
            {
            new int[] { 1, 2, 3, 4, 5 },
            new int[] { 2, 3, 4, 5, 6 },
            new int[] { 3, 4, 5, 6, 7 },
            };

            this.initialDataIntIncorrectColumns = new int[][]
            {
            new int[] { 1, 2, 3, 4, 5 },
            new int[] { 2, 3, 5, 6 },
            new int[] { 3, 4, 5, 6, 7 },
            new int[] { 6, 7, 8 },
            new int[] { 5, 6, 7, 8 }
            };
        }

        [Test]
        public void ConstructorTest()
        {
            var squareMatrix = new SquareMatrix<int>(intialDataInt1);

            Assert.AreEqual(squareMatrix.SizeOfSide, intialDataInt1.Length);
        }

        [Test]
        public void ConstructorTestsThrowArgumentExceptionIncorrectRows()
        {
            Assert.Throws<ArgumentException>(() => new SquareMatrix<int>(this.initialDataIntIncorrectRows));
        }

        [Test]
        public void ConstructorTestsThrowArgumentExceptionIncorrectColumns()
        {
            Assert.Throws<ArgumentException>(() => new SquareMatrix<int>(this.initialDataIntIncorrectRows));
        }

        [Test]
        public void EventTest()
        {
            // Arrange
            var squareMatrix = new SquareMatrix<int>(intialDataInt1);
            int i = 2, j = 3, element = 4;

            bool IsReplaced = false;
            ReplacementEventArgs<int> result = null;

            squareMatrix.Replaced += (object sender, ReplacementEventArgs<int> args) =>
              {
                  IsReplaced = true;
                  result = args;
              };

            // Act
            squareMatrix[i, j] = element;

            // Assert
            Assert.AreEqual(true, IsReplaced);
            Assert.AreNotEqual(null, result);
            Assert.AreEqual(i, result.I);
            Assert.AreEqual(j, result.J);
            Assert.AreEqual(element, result.CurrentValue);
        }
    }
}