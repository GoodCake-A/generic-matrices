﻿using NUnit.Framework;
using GenericMatrices.EventArguments;
using System;
using System.Linq.Expressions;

namespace GenericMatrices.Test
{
    public class SquareMatrixExtensionTests
    {
        private int[][] intialDataInt;

        private string[][] initialDataString;

        private string[][] result;

        [SetUp]
        public void Setup()
        {
            this.intialDataInt = new int[][]
            {
            new int[] { 1, 2, 3 },
            new int[] { 2, 3, 4 },
            new int[] { 3, 4, 5 },
            };

            this.initialDataString = new string[][]
            {
                new string[]{ "one", "two", "three" },
                new string[]{ "two", "three", "four" },
                new string[]{ "three", "four", "five" }
            };

            this.result = new string[][]
            {
                new string[]{ "one1", "two2", "three3" },
                new string[]{ "two2", "three3", "four4" },
                new string[]{ "three3", "four4", "five5" }
            };
        }

        [Test]
        public void AddictionTest()
        {
            var squareMatrixWithInts = new SquareMatrix<int>(this.intialDataInt);
            var squareMatrixWithStrings = new SquareMatrix<string>(this.initialDataString);



            var firstArgument = Expression.Parameter(typeof(string));
            var secondArgument = Expression.Parameter(typeof(int));

            
            var toStringMethod = typeof(int).GetMethod(nameof(int.ToString), new Type[] { });
            var secondAsString = Expression.Call(secondArgument, toStringMethod);

            var sum = Expression.Call(typeof(string).GetMethod(nameof(string.Concat),new Type[] { typeof(string),typeof(string)}),
                firstArgument, secondAsString);

            var resultExpression = Expression.Lambda<Func<string, int, string>>(sum, firstArgument, secondArgument);


            var matricesSum = squareMatrixWithStrings.Add<string, int, string>(squareMatrixWithInts, resultExpression);

            for(int i =0;i<matricesSum.SizeOfSide;i++)
            {
                for(int j=0;j<matricesSum.SizeOfSide;j++)
                {
                    Assert.AreEqual(result[i][j], matricesSum[i, j]);
                }
            }

        }

    }
}