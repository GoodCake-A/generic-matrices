﻿using NUnit.Framework;
using GenericMatrices.EventArguments;
using System;

namespace GenericMatrices.Test
{
    public class DioganalMatrixTests
    {
        private int[] intialDataInt;

        [SetUp]
        public void Setup()
        {
            this.intialDataInt = new int[]{ 1, 2, 3, 4, 5 };

        }

        [Test]
        public void ConstructorTest()
        {
            var dioganalMatrix = new DioganalMatrix<int>(intialDataInt);

            int[] result = new int[dioganalMatrix.SizeOfSide];
            for(int i=0;i<dioganalMatrix.SizeOfSide; i++)
            {
                result[i] = dioganalMatrix[i, i];
            }

            Assert.AreEqual(dioganalMatrix.SizeOfSide, intialDataInt.Length);
            Assert.AreEqual(dioganalMatrix[1, 2], default(int));
            Assert.AreEqual(intialDataInt, result);
        }
    }
}