﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using GenericMatrices.EventArguments;

namespace GenericMatrices
{
    public class SquareMatrix<TElement>
    {
        private readonly TElement[][] data;

        public SquareMatrix(int sizeOfSide)
        {
            if (sizeOfSide <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sizeOfSide));
            }

            this.data = new TElement[sizeOfSide][];

            for (int j = 0; j < this.data.Length; j++)
            {
                this.data[j] = new TElement[sizeOfSide];
            }
        }

        public SquareMatrix(TElement[][] initialElements)
        {
            int sizeOfSide = initialElements.Length;
            if (sizeOfSide <= 0)
            {
                throw new ArgumentException($"{nameof(initialElements)} must be array of arrays [n][n].");
            }

            this.data = new TElement[sizeOfSide][];

            for (int j = 0; j < this.data.Length; j++)
            {
                this.data[j] = new TElement[sizeOfSide];
            }

            for (int i = 0; i < sizeOfSide; i++)
            {
                if (initialElements[i].Length != sizeOfSide)
                {
                    throw new ArgumentException($"{nameof(initialElements)} must be array of arrays [n][n].");
                }

                for (int j = 0; j < sizeOfSide; j++)
                {
                    this.data[i][j] = initialElements[i][j];
                }
            }
        }

        public SquareMatrix(IEnumerable<IEnumerable<TElement>> initialElements)
            : this((from iElement in initialElements
                    select iElement.ToArray()).ToArray())
        {
        }

        public event EventHandler<ReplacementEventArgs<TElement>> Replaced;

        public TElement this[int i, int j]
        {
            get
            {
                if(i<0||i>=this.SizeOfSide)
                {
                    throw new ArgumentOutOfRangeException(nameof(i));
                }

                if (j < 0 || j >= this.SizeOfSide)
                {
                    throw new ArgumentOutOfRangeException(nameof(j));
                }

                return this.data[i][j];
            }

            set
            {
                if (i < 0 || i >= this.SizeOfSide)
                {
                    throw new ArgumentOutOfRangeException(nameof(i));
                }

                if (j < 0 || j >= this.SizeOfSide)
                {
                    throw new ArgumentOutOfRangeException(nameof(j));
                }

                var oldElementValue = this.data[i][j];
                this.data[i][j] = value;

                var replacementInfo = new ReplacementEventArgs<TElement> { CurrentValue = value, OldValue = oldElementValue, I = i, J = j };
                this.Replaced?.Invoke(this, replacementInfo);
            }
        }

        public int SizeOfSide
        {
            get { return this.data.Length; }
        }
    }
}
