﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericMatrices.EventArguments
{
    public class ReplacementEventArgs<TElement>:EventArgs
    {
        public TElement OldValue { get; set; }

        public TElement CurrentValue { get; set; }

        public int I { get; set; }

        public int J { get; set; }
    }
}
