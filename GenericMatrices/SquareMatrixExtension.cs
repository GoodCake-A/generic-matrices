﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;

namespace GenericMatrices
{
    public static class SquareMatrixExtension
    {
        public static SquareMatrix<TResult> Add<TElement1, TElement2, TResult>(this SquareMatrix<TElement1> summand1, SquareMatrix<TElement2> summand2, Expression<Func<TElement1,TElement2,TResult>> adder)
        {
            if(summand1 is null)
            {
                throw new ArgumentNullException(nameof(summand1));
            }

            if(summand2 is null)
            {
                throw new ArgumentNullException(nameof(summand2));
            }

            if(adder is null)
            {
                throw new ArgumentNullException(nameof(adder));
            }

            if(summand1.SizeOfSide!=summand2.SizeOfSide)
            {
                throw new ArgumentException($"{nameof(summand1)} and {nameof(summand2)} must have same side length ");
            }

            var compiledAdder = adder.Compile();

            var result = new SquareMatrix<TResult>(summand1.SizeOfSide);

            for(int i=0;i<result.SizeOfSide;i++)
            {
                for(int j=0;j<result.SizeOfSide;j++)
                {
                    result[i, j] = compiledAdder(summand1[i, j], summand2[i, j]);
                }
            }

            return result;
        }
    }
}
