﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericMatrices
{
    public class DioganalMatrix<TElement>:SquareMatrix<TElement>
    {
        public DioganalMatrix(TElement[] initialElements) : base(DiogonalMatrixInputToSquareMatrixInput(initialElements))
        {
        }

        private static TElement[][] DiogonalMatrixInputToSquareMatrixInput(TElement[] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length < 1)
            {
                throw new ArgumentException($"{nameof(source)} must be array [n] of arrays with increasing length from 1 to n.");
            }

            TElement[][] squareMatrixInput = new TElement[source.Length][];

            for (int i = 0; i < source.Length; i++)
            {
                squareMatrixInput[i] = new TElement[source.Length];
            }

            for (int i = 0; i < source.Length; i++)
            {
                squareMatrixInput[i][i] = source[i];
            }

            return squareMatrixInput;
        }
    }
}
