﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericMatrices
{
    public class SymmetricMatrix<TElement> : SquareMatrix<TElement>
    {
        public SymmetricMatrix(TElement[][] initialElements) : base(SymmetricMatrixInputToSquareMatrixInput(initialElements))
        {
        }

        private static TElement[][] SymmetricMatrixInputToSquareMatrixInput(TElement[][] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length < 1)
            {
                throw new ArgumentException($"{nameof(source)} must be array [n] of arrays with increasing length from 1 to n.");
            }

            TElement[][] squareMatrixInput = new TElement[source.Length][];

            for (int i = 0; i < source.Length; i++)
            {
                squareMatrixInput[i] = new TElement[source.Length];
            }

            for (int i = 0; i < source.Length; i++)
            {
                if (source[i].Length != source.Length - i)
                {
                    throw new ArgumentException($"{nameof(source)} must be array [n] of arrays with increasing length from 1 to n.");
                }

                for (int j = 0, positionInMatrixRow = j; positionInMatrixRow < source.Length - i; j++, positionInMatrixRow++)
                {
                    squareMatrixInput[i][positionInMatrixRow] = source[i][j];
                    squareMatrixInput[positionInMatrixRow][i] = source[i][j];
                }
            }

            return squareMatrixInput;
        }
    }
}
